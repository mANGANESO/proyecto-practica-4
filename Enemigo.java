import javax.swing.*;
import java.awt.image.*;
import java.awt.*;
//Desarrollo del enemigo
class Enemigo
{
	BufferedImage enemigo;

	public Enemigo(BufferedImage enemigo)
	{
		this.enemigo = enemigo;
	}

	@Override
	public void paintComponent(Graphics s)
	{
		super.paintComponent(s);
		s.drawImage(enemigo,100,70,64,64,null);
	}
}