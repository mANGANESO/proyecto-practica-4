import javax.sound.midi.*;
import java.util.*;
class MiReproductor
{
private Synthesizer synthe = null;
private int intensity = 100;
private MidiChannel [] channels;
private MidiChannel channel;
public MiReproductor()
{
try
{
synthe =  MidiSystem.getSynthesizer();
}
catch (Exception e)
{
System.out.println("ERROR: al obtener el synthe");
}
}
public void inicializar()
{
try
{
synthe.open();
channels = synthe.getChannels();
}
catch(Exception e)
{
System.out.println("ERROR: al inicializar el synthe");
}
}
public void reproducirNota(int nota, int canal, int duracion)
{
channel = channels [canal];
channel.noteOn(nota, intensity);
try
{
Thread.sleep(duracion);
}
catch ( Exception e)
{
System.out.println("ERROR: en sleep.");
e.printStackTrace();
}
channel.noteOff(nota);
}
public void reproducirAcordeTresNotas(int nota1, int nota2,int nota3, int canal, int duracion)
{
channel = channels [canal];
channel.noteOn(nota1, intensity);
channel.noteOn(nota2, intensity);
channel.noteOn(nota3, intensity);
try
{
Thread.sleep(duracion);
}
catch ( Exception e)
{
System.out.println("ERROR: en sleep.");
e.printStackTrace();
}
channel.noteOff(nota1);
channel.noteOff(nota2);
channel.noteOff(nota3);
}
public void reproducirAcordeNotas(ArrayList<Integer> notas,int n, int canal, int duracion)
{
channel = channels [canal];
for (int i = 0;i<n ;i++ )
    channel.noteOn(notas.get(i), intensity);
try
{
Thread.sleep(duracion);
}
catch ( Exception e)
{
System.out.println("ERROR: en sleep.");
e.printStackTrace();
}
for (int i = 0; i<n; i++ )
    channel.noteOff(notas.get(i), intensity);
}
public void finalizar()
{
try
{
synthe.close();
}
catch(Exception e)
{
System.out.println("ERROR: al finalizar el synthe. ");
}
}
public void cambiarCanal(int aux){
   System.out.println("ha sido cambiado el canal");
}
}