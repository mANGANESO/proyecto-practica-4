import java.awt.*;
import javax.swing.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;
import java.awt.event.*;

class Carga implements Runnable
{
	Enemigo1 esqueleto;
	BufferedImage enemigo1;
	
	public Carga(Enemigo1 esqueleto,BufferedImage enemigo1)
	{
		this.esqueleto = esqueleto;
		this.enemigo1 = enemigo1;
	}
	
	@Override
	public void run()
	{
		int x = 100;
		int y = 70;

		for (int z=0; z<2;z++)
		{
			for(int i=0; i<=8; i++)
			{
				int a = i+1;
				x=x+a;
				//System.out.println("Prueba Hilo: "+a);
				this.esqueleto.enemigo1 = enemigo1.getSubimage(a*64,64*17,64,64);
				this.esqueleto.setLocation(x,y);
				retardo(100);
				x=x-6;
			}
			x=100;
			z=0;
		}
	}
		
	public void retardo(int ms)
	{
		try
		{
			Thread.sleep(ms);
		}
		catch(Exception e)
		{
			System.out.println("Error: al ejecuar el sleep.");
		}
	}
}