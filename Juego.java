import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.lang.*;
import java.io.*;
import javax.imageio.*;
import java.awt.image.*;
//import java.awt.GridLayout;

///
// Videojuego creado en 2d
///    <>
class Juego
{
	public static void main(String[] args) 
  {
		Portada menu = new Portada();
    FondoMusical musica = new FondoMusical();

	}
	///Este sera el inicio
	public static class Portada extends JFrame implements ActionListener, KeyListener
  {
    JPanel panel;
    JLabel portada;
    JLabel fondo01;
    JLabel fondo02;
    JLabel fondo03;
    JLabel fondo04;
    JLabel fondo05;
    BufferedImage bfimage;
    BufferedImage fondo1;
    BufferedImage subFondo1;
    BufferedImage fondo2;
    BufferedImage subFondo2;
    BufferedImage fondo3;
    BufferedImage subFondo3;
    BufferedImage fondo4;
    BufferedImage subFondo4;
    BufferedImage fondo5;
    BufferedImage subFondo5;
    JButton btninicio;
    JButton btntest;
    BufferedImage avatar;
    BufferedImage subavatar;
    BufferedImage enemigos;
    BufferedImage enemigo;
    BufferedImage subEnemigo;
    int indiceX = 0;
    int indiceX2 = 9;
    int indiceX3 = 0;
    int indiceX4 = 0;
    int indiceAX = 0;
    int indiceAX2 = 0;
    int indiceAX3 = 0;
    int indiceAX4 = 0;

    Thread hilo1;
    Avatar personajeP;
    Enemigo1 esqueleto;

    //panel
    public Portada()
    {
      
      componentesPaneles();

      this.add(panel);
      this.setSize(1200,600);
      this.setVisible(true);
      this.setTitle("Thunder Game");
      this.setLocationRelativeTo(null);
      this.setDefaultCloseOperation(this.EXIT_ON_CLOSE);
      this.getContentPane().setBackground(Color.BLACK);
      this.setResizable(false);
      this.addKeyListener(this);
       
      btninicio.addActionListener(this);
    }

    public void componentesPaneles()
    {

      panel = new JPanel();
      panel.setLayout(null);
      panel.setFocusable(true);
      panel.requestFocusInWindow();
      panel.setBackground(Color.BLACK);
      panel.setVisible(true);
      //boton de inicio
      try
      {
        bfimage = ImageIO.read(new File("./images/Favicon.png"));   
        portada = new JLabel(new ImageIcon(bfimage));   
        portada.setBounds(0,0,1200,600);
        btninicio= new JButton("Comenzar");
        btninicio.setBounds(544,400,200,50);
        btninicio.setForeground(Color.WHITE);
        btninicio.setOpaque(true);
        btninicio.setBackground(Color.RED);
      }

      catch(Exception e)
      {
      	System.out.println("Error de boton o portada :(");
      }
      panel.add(btninicio);
      panel.add(portada);
    }

    //si presiona boton
    public void actionPerformed(ActionEvent event)
    {
      if (event.getSource() == btninicio)
      {
        panel.setVisible(false);
        this.requestFocus();
        remove(panel);

        try
        {
         avatar = ImageIO.read(new File("./images/avatar.png"));
        }
        catch(Exception e)
        {
          System.out.println("Ha ocurrido un error a la hora de cargar los jugadores. :(");
        } 

        subavatar = avatar.getSubimage(0,64 * 3, 64, 64);
        personajeP = new Avatar(subavatar);
        personajeP.setOpaque(false);
        this.add(personajeP);

        try
        {
          enemigo = ImageIO.read(new File("./images/esqueletoarquero.png"));
        }
        catch(Exception e)
        {
          System.out.println("Error al cargar el enemigo");
        }

        subEnemigo = enemigo.getSubimage(0,64*17,64,64);
        esqueleto = new Enemigo1(subEnemigo);
        esqueleto.setOpaque(false);
        this.add(esqueleto);
        crearFondo();
        crearHilo();
      }
    }

    //si pone enter
    public void keyPressed(KeyEvent e)
    {
      int t = e.getKeyCode();
      Point pos = personajeP.getLocation();

      int x = (int)pos.getX();
      int y = (int)pos.getY();
      try
      {
        //desplazamiento hacia abajo  
        if(t==68)
        {
          x = x+5;
          indiceX = indiceX + 1;
          personajeP.avatar = avatar.getSubimage(indiceX*64,64*11,64,64);
          if (indiceX == 8) 
          {
           indiceX = 0;
          }
          else if(x > 1180)
          {
            x=0;
          }
        }
        //desplazamiento hacia arrba
        else if(t==65)
        {
          x = x-5;
          indiceX2 =  indiceX2 - 1;
          personajeP.avatar = avatar.getSubimage(indiceX2*64,64*9,64,64);
          if (indiceX2 == 0) 
          {
           indiceX2 =9;
          }
          else if (x<0) 
          {
           x=0;
          }
        }
        //Desplazamiento hacia abajo
        else if(t==83)
        {
          y = y+5;
          indiceX3 = ((indiceX3 + 1)% 9)*64;
          personajeP.avatar = avatar.getSubimage(indiceX3,64*10,64,64);
          if (y > 300) 
          {
           y = 300;
          } 
        }
        //desplazamiento hacia arriba
        else if(t==87)
        {
          y = y-5;
          indiceX4 = ((indiceX4 + 1)%9)*64;
          personajeP.avatar = avatar.getSubimage(indiceX4,64*8,64,64);
          if (y<-214) 
          {
           y=-214;
          }
        }
        //Pirueta
        else if(t == 82 )
        {
          y = y+15;
          indiceAX = ((indiceAX + 1)%6)*64;
          personajeP.avatar = avatar.getSubimage(indiceAX,64*20,64,64);
          if (y > 300) 
          {
            y = 300;
          } 
        } 
        //Golpe 
        else if(t == KeyEvent.VK_ENTER )
        {   
          x = x+1;
          indiceAX2 = ((indiceAX2 + 1)%7)*64;
          personajeP.avatar = avatar.getSubimage(indiceAX2,64*3,64,64);  
        } 
        personajeP.setLocation(x,y);
      }
      catch(Exception a)
      {
        System.out.println("Error en movimiento");
      }
    }

    public void keyReleased(KeyEvent e){}
    public void keyTyped(KeyEvent e){}

    public void crearHilo()
    {
      Carga oh1 = new Carga(this.esqueleto,this.enemigo);
      hilo1 = new Thread(oh1);
      hilo1.start();
    }

    public void crearFondo()
    {
      this.setLayout(new GridLayout());
      try
      {
        fondo1 = ImageIO.read(new File("./images/fondos.png"));
        subFondo1 = fondo1.getSubimage(0,0, 160, 190);   
        fondo01 = new JLabel(new ImageIcon(subFondo1));   
        fondo01.setBounds(0,110,600,800);
        fondo2 = ImageIO.read(new File("./images/fondos.png"));
        subFondo2 = fondo2.getSubimage(176,0, 126, 190);   
        fondo02 = new JLabel(new ImageIcon(subFondo2));   
        fondo02.setBounds(800,110,600,800);
      }
      catch(Exception e)
      {
        System.out.println("Error en el fondo :(");
      }
      this.add(fondo01);
      this.add(fondo02);
    }
  }
}