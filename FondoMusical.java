import java.io.*; 
import javax.sound.midi.*; 
import java.util.*; 
class FondoMusical{
 Archivo archivo;
 MiReproductor mr;
 ArrayList<String> contenido;
 int canal = 1;	

 public FondoMusical(){
 
 contenido = new ArrayList<String>();	
 archivo = new Archivo();	
 mr = new MiReproductor(); 


 while(true){  

  mr.inicializar();
   contenido = archivo.leerTodo("Thunder.txt");
    mr.inicializar();
     for(String i : contenido)
       {                            
        String nota = i.substring(2,4);                           
        String duracion = i.substring(7,10);                          
        int notaInt = Integer.parseInt(nota);                          
        int duracionInt = Integer.parseInt(duracion); 
                                 
        mr.reproducirNota(notaInt, canal, duracionInt);                       
       }
   mr.finalizar();   
  }
  
 }
}